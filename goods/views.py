from rest_framework import generics
from .models import Brand, GoodsCategory, Goods
from .serializers import BrandSerializer, GoodsCategorySerializer, GoodsSerializer

# работа с брендами
class BrandListCreateAPIView(generics.ListCreateAPIView):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer

class BrandDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer

class BrandCreateAPIView(generics.CreateAPIView):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer

class BrandUpdateAPIView(generics.UpdateAPIView):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer

class BrandDeleteAPIView(generics.DestroyAPIView):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer

# работа с категориями
class GoodsCategoryListCreateAPIView(generics.ListCreateAPIView):
    queryset = GoodsCategory.objects.all()
    serializer_class = GoodsCategorySerializer

class GoodsCategoryDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = GoodsCategory.objects.all()
    serializer_class = GoodsCategorySerializer

class GoodsCategoryCreateAPIView(generics.CreateAPIView):
    queryset = GoodsCategory.objects.all()
    serializer_class = GoodsCategorySerializer

class GoodsCategoryUpdateAPIView(generics.UpdateAPIView):
    queryset = GoodsCategory.objects.all()
    serializer_class = GoodsCategorySerializer

class GoodsCategoryDeleteAPIView(generics.DestroyAPIView):
    queryset = GoodsCategory.objects.all()
    serializer_class = GoodsCategorySerializer

# работа с товарами
class GoodsListCreateAPIView(generics.ListCreateAPIView):
    queryset = Goods.objects.all()
    serializer_class = GoodsSerializer

class GoodsDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Goods.objects.all()
    serializer_class = GoodsSerializer

class GoodsCreateAPIView(generics.CreateAPIView):
    queryset = Goods.objects.all()
    serializer_class = GoodsSerializer

class GoodsUpdateAPIView(generics.UpdateAPIView):
    queryset = Goods.objects.all()
    serializer_class = GoodsSerializer

class GoodsDeleteAPIView(generics.DestroyAPIView):
    queryset = Goods.objects.all()
    serializer_class = GoodsSerializer

class GoodsByCategoryView(generics.ListAPIView):
    serializer_class = GoodsSerializer

    def get_queryset(self):
        category_id = self.kwargs['category_id']
        return Goods.objects.filter(category_id=category_id)

class GoodsByBrandView(generics.ListAPIView):
    serializer_class = GoodsSerializer

    def get_queryset(self):
        brand_id = self.kwargs['brand_id']
        return Goods.objects.filter(brand_id=brand_id)

