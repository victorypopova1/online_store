from django.db import models

class Brand(models.Model):
    name = models.TextField()
    icon = models.TextField(null=True)

class GoodsCategory(models.Model):
    name = models.TextField()
    icon = models.TextField(null=True)

class Goods(models.Model):
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    category = models.ForeignKey(GoodsCategory, on_delete=models.CASCADE)
    name = models.TextField()
    price = models.FloatField()
    rating = models.IntegerField()
    description = models.TextField()
    image = models.TextField(null=True)